
execVM "briefing.sqf";

if (isDedicated) then {
	"Dan_SkipTime1" addPublicVariableEventHandler {
		SkipTime 1
	};

	"Dan_SkipTime3" addPublicVariableEventHandler {
		SkipTime 3
	};

	"Dan_SkipTime6" addPublicVariableEventHandler {
		SkipTime 6
	};

	"Dan_SkipTime12" addPublicVariableEventHandler {
		SkipTime 12
	};
};

////Completed Task Markers ////

"Completed_1" setMarkerAlpha 0;
"Completed_2" setMarkerAlpha 0;
"Completed_3" setMarkerAlpha 0;
"Completed_4" setMarkerAlpha 0;
"Completed_5" setMarkerAlpha 0;
"Completed_6" setMarkerAlpha 0;
"Completed_7" setMarkerAlpha 0;
"Completed_8" setMarkerAlpha 0;
"Completed_9" setMarkerAlpha 0;
"Completed_10 "setMarkerAlpha 0; 

"Failed" setMarkerAlpha 0;
